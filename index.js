'use strict';

const Yii2Controller = require('./generators/yii2-controller');
const Yii2Widget = require('./generators/yii2-widget');
const Yii2Component = require('./generators/yii2-component');

class Generator {

    register(programme) {
        new Yii2Component(programme);
        new Yii2Controller(programme);
        new Yii2Widget(programme);
    }

};

module.exports = new Generator;
