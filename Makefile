PWD=$(shell pwd)
NODE=${PWD}/node_modules/.bin

CMD=

all: lint test

install:
	npm install;

lint:
	${NODE}/eslint generators test;
	${NODE}/validate-npm-package;

test:
	${NODE}/mocha --recursive ${CMD};
.PHONY: test

coverage:
	${NODE}/nyc ${NODE}/mocha --recursive;
	${NODE}/nyc report --reporter=html;
.PHONY: coverage

clean:
	rm -rf ${PWD}/.nyc_output ${PWD}/coverage
