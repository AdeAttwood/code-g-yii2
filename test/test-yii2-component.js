/* global it, describe, beforeEach, afterEach */
'use strict';

/**
 * Test the generation of a yii2 component
 *
 * @author  Ade Attwood <code@adeattwood.co.uk>
 * @since   @next_version@
 */

const assert = require('code-g/test/assert');
const Command = require('commander').Command;
const Fs = require('fs');

const Yii2Component = require('../generators/yii2-component');

describe('yii2-component', function() {

    beforeEach(() => {
        this.app = new Command('Test Command');
        new Yii2Component(this.app);
    });

    afterEach(() => {
        Fs.unlinkSync('TestingComponent.php');
    });

    it('Should create a yii2 component', () => {
        this.app.commands[0].name = 'TestingComponent';
        this.app.commands[0].path = process.cwd();
        this.app.emit('command:yii2:component');

        assert.fileContains('TestingComponent.php', 'public function init()');
        assert.fileExists('TestingComponent.php');
    });

});
