'use strict';

const CodeGen = require('code-gen');

class Yii2Component {

    constructor(programme) {
        let command = programme.command('yii2:component');

        command.description('Generates a Yii2 component');
        command.action(this.action);

        command.option('-n, --name <value>', 'The name of the component');
        command.option(
            '-p, --path <value>',
            'The path to save the component',
            CodeGen.Arguments.isDir,
            process.cwd() + '/components'
        );

        this.command = command;
    }

    static generate() {
        let namespace = this.path
            .replace(process.cwd(), '')
            .replace(/\//g, '\\');

        if (!namespace.startsWith('app')) {
            namespace = 'app' + namespace;
        }

        let generator = new CodeGen.Generator({
            'templateName': 'php-class',
            'name': this.name,
            'basePath': this.path,
            'fileExtension': 'php',
            'args': {
                'class_name': this.name,
                'extends': '\\yii\\base\\Component',
                'namespace': namespace,
                'methods': [
                    {
                        access: 'public',
                        name: 'init',
                        returns: 'void',
                    }
                ]
            }
        });

        generator.basePath = this.path;
        generator.save();

        return generator.save();
    }

    action() {
        if (!this.name) {
            CodeGen.Output.error('-p, --path must be a valid dir');
            process.exit(1);
        }

        return Yii2Component.generate.apply(this);
    }

}

module.exports = Yii2Component;
