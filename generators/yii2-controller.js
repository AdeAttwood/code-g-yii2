'use strict';

const CodeGen = require('code-gen');

class Yii2Controller {

    constructor(programme) {
        let command = programme.command('yii2:controller');

        command.description('Generates a Yii2 controller');
        command.action(this.action);

        command.option('-n, --name <value>', 'The name of the class');
        command.option(
            '-p, --path <value>',
            'The path to save the class',
            CodeGen.Arguments.isDir,
            process.cwd() + '/controllers'
        );

        command.option(
            '-a, --actions [value]',
            'The name of the action to add to the controller',
            CodeGen.Arguments.collect,
            []
        );

        this.command = command;
    }

    static generate() {
        let namespace = this.path
            .replace(process.cwd(), '')
            .replace(/\//g, '\\');

        if (!namespace.startsWith('app')) {
            namespace = 'app' + namespace;
        }

        let _actions  = [];
        for (let action of this.actions) {
            _actions.push({
                access: 'public',
                name: action
            });
        }

        let generator = new CodeGen.Generator({
            'templateName': 'php-class',
            'name': this.name,
            'basePath': this.path,
            'fileExtension': 'php',
            'args': {
                'class_name': this.name,
                'extends': '\\yii\\web\\Controller',
                'namespace': namespace,
                'methods': _actions
            }
        });

        generator.basePath = this.path;
        generator.save();

        return generator.save();
    }

    action() {
        if (!this.name) {
            CodeGen.Output.error('-p, --path must be a valid dir');
            process.exit(1);
        }

        return Yii2Controller.generate.apply(this);
    }

}

module.exports = Yii2Controller;
