'use strict';

const CodeGen = require('code-gen');

class Yii2Widget {

    constructor(programme) {
        let command = programme.command('yii2:widget');

        command.description('Generates a Yii2 widget');
        command.action(this.action);

        command.option('-n, --name <value>', 'The name of the widget');
        command.option(
            '-p, --path <value>',
            'The path to save the widget',
            CodeGen.Arguments.isDir,
            process.cwd() + '/widgets'
        );

        this.command = command;
    }

    static generate() {
        let namespace = this.path
            .replace(process.cwd(), '')
            .replace(/\//g, '\\');

        if (!namespace.startsWith('app')) {
            namespace = 'app' + namespace;
        }

        let generator = new CodeGen.Generator({
            'templateName': 'php-class',
            'name': this.name,
            'basePath': this.path,
            'fileExtension': 'php',
            'args': {
                'class_name': this.name,
                'extends': '\\yii\\base\\Widget',
                'namespace': namespace,
                'methods': [
                    {
                        access: 'public',
                        name: 'init',
                        returns: 'void',
                    },
                    {
                        access: 'public',
                        name: 'run',
                        returns: 'string',
                    }
                ]
            }
        });

        generator.basePath = this.path;
        generator.save();

        return generator.save();
    }

    action() {
        if (!this.name) {
            CodeGen.Output.error('-p, --path must be a valid dir');
            process.exit(1);
        }

        return Yii2Widget.generate.apply(this);
    }

}

module.exports = Yii2Widget;
