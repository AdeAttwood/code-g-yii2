/* global it, describe, beforeEach, afterEach */
'use strict';

/**
 * Test the generation of a yii2 widget
 *
 * @author  Ade Attwood <code@adeattwood.co.uk>
 * @since   @next_version@
 */

const assert = require('code-g/test/assert');
const Command = require('commander').Command;
const Fs = require('fs');

const Yii2Widget = require('../generators/yii2-widget');

describe('yii2-widget', function() {

    beforeEach(() => {
        this.app = new Command('Test Command');
        new Yii2Widget(this.app);
    });

    afterEach(() => {
        Fs.unlinkSync('TheWidget.php');
    });

    it('Should create a yii2 widget', () => {
        this.app.commands[0].name = 'TheWidget';
        this.app.commands[0].path = process.cwd();
        this.app.emit('command:yii2:widget');

        assert.fileContains('TheWidget.php', 'public function init()');
        assert.fileContains('TheWidget.php', 'public function run()');
        assert.fileExists('TheWidget.php');
    });

});
