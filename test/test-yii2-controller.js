/* global it, describe, beforeEach, afterEach */
'use strict';

/**
 * Test the generation of a yii2 controller
 *
 * @author  Ade Attwood <code@adeattwood.co.uk>
 * @since   @next_version@
 */

const assert = require('code-g/test/assert');
const Command = require('commander').Command;
const Fs = require('fs');

const Yii2Controller = require('../generators/yii2-controller');

describe('yii2-controller', function() {

    beforeEach(() => {
        this.app = new Command('Test Command');
        new Yii2Controller(this.app);
    });

    afterEach(() => {
        Fs.unlinkSync('TestingController.php');
    });

    it('Should create a yii2 controller', () => {
        this.app.commands[0].name = 'TestingController';
        this.app.commands[0].path = process.cwd();
        this.app.emit('command:yii2:controller');

        assert.fileExists('TestingController.php');
    });

});
